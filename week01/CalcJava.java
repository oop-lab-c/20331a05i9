import java.util.*;
public class Main
{
	public static void main(String[] args) {
		System.out.print("Enter two numbers:");
		Scanner  input=new Scanner(System.in);
		float n1=input.nextFloat();
		float n2=input.nextFloat();
		System.out.print("Enter operator:");
		char op=input.next().charAt(0);
	    if(op=='+')
	    System.out.println(n1+n2);
	    if(op=='-')
	    System.out.println(n1-n2);
	    if(op=='*')
	    System.out.println(n1*n2);
	    if(op=='/')
	    System.out.println(n1/n2);
	    if(op=='%')
	    System.out.println(n1%n2);
	    else
	    System.out.println("Invalid operator");
	}
}
