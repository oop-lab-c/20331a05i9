//Demonstrate Simple Inheritance using a real time example.
class Templete{
    void wheel(){
        System.out.println("wheels are round");
    }
}
class car extends Templete{
    void design(){
        System.out.println("Car is Comfortable");
    }
}
class SimpleInher{
    public static void main(String[] args) {
        car obj = new car();
        obj.wheel();
        obj.design();
    }
}