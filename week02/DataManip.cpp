#include<iostream>
#include<iomanip>
using namespace std;
void endlfun(){
    cout<<"endl use"<<endl;
    cout<<"line1"<<endl<<"line2"<<endl;
}
void endsfun(){
    cout<<"ends use"<<endl;
cout<<'a'<<ends;
cout<<'b'<<ends;
cout<<'c'<<endl;
}
//void wsfun(){}
void flushfun(){
    cout<<"flush use"<<endl;
cout<<"example of flush"<<flush<<endl;
}
void setwfun(){
    cout<<"setw use"<<endl;
    cout<<setw(10)<<1111<<endl;
    cout<<setw(10)<<123<<endl;
}
void setfillfun(){
    cout<<"setfill use"<<endl;
    cout<<setfill('$')<<setw(5)<<12<<endl;
    cout<<setfill('#')<<setw(5)<<1<<endl;
}
void setprecisionfun(){
    cout<<"setpresition use"<<endl;
    cout<<setprecision(5)<<123.456788<<endl;
    cout<<setprecision(7)<<123.456788<<endl;
}
int main(){
    endlfun();
    endsfun();
    //wsfun();
    flushfun();
    setwfun();
    setfillfun();
    setprecisionfun();

}